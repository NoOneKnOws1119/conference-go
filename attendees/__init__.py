class Mammal:
    def __init__(self, temperature, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.temperature = temperature

class Flyer:
    altitude = 0

    def flap(self):
        self.altitude += 10

    def dive(self):
        self.altitude -= 10

class Bat(Mammal, Flyer):
    pass

print(Mammal(90))
