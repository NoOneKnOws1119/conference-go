from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location
from django.views.decorators.http import require_http_methods
class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ['name']


def api_list_conferences(request):

    conferences = Conference.objects.all()
    return JsonResponse(
        {'conferences': conferences},
        encoder=ConferenceListEncoder,
    )

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ['name']


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        'location',
    ]
    encoders = {
        'location': LocationListEncoder(),
    }
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    conference = Conference.objects.get(id=id)
    return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
    )



@require_http_methods(["Get", "Post"])
def api_list_locations(request):
    locations = Location.objects.all()
    return JsonResponse(
        {"locations": locations},
        encoder=LocationListEncoder,
    )

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        'name',
        'city',
        'room_count',
        'created',
        'updated',
    ]

    def get_extra_data(self, o):
        return {'state': o.state.abbreviation}


def api_show_location(request, id):
    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )
